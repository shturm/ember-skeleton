ember-skeleton
==============

Ember.js skeleton application + qunit, tests runnable with karma and qunit runner

## Running

* Extract contents of `vendor.tar.gz` and `node_modules.tar.gz` to he root folder OR run: 
```
npm install
```

* Open `index.html`

## Testing
```
karma start
```